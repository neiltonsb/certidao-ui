export const environment = {
  production: true,

  apiUrl: 'http://srvcontabil.sefin.ro.gov.br/certidao-api/',

  manutencao: {
    status: false,
    message: 'Estamos em manutencao para alguns ajustes tecnicos. Favor, tentar novamente mais tarde. Agradecemos a compreensão'
  }
};
