import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

import { MessageService } from 'primeng/components/common/messageservice';

@Injectable()
export class ErrorHandlerService {

  constructor(
    private messageService: MessageService,

    private router: Router
  ) { }

  handle(errorResponse: any) {
    this.messageService.clear();
    
    let msg: string;
    msg = 'Ocorreu um erro ao processar a sua solicitação';
    
    if(typeof errorResponse === 'string') {
      msg = errorResponse;
    } else if (errorResponse instanceof HttpErrorResponse) {
      if (errorResponse.status === 400) {
        msg = errorResponse.error[0].message;
      }

      if (errorResponse.status === 404) {
        msg = 'Não encontramos o recurso que você solicitou';

        if (errorResponse.error instanceof Array 
           && errorResponse.error[0].type === 'CertidaoNaoEncontradaException') {
            msg = 'Certidão não encontrada, verifique as informações inseridas';
           }
      }

      if (errorResponse.status === 403) {
        msg = 'Você não tem permissão para executar esta ação';
      }

      if (errorResponse.status === 500) {
        this.router.navigate(['error']);
        return;
      }
    }

    console.log('Ocorreu um erro', errorResponse);

    this.messageService.add({ severity: 'error', detail: msg });
  }
}
