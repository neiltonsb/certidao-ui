import { RouterModule } from '@angular/router';
import localePt from '@angular/common/locales/pt';
import { Title } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule, registerLocaleData } from '@angular/common';


import { MessageService } from 'primeng/components/common/messageservice';
import { MessagesModule } from 'primeng/messages';


import { CertidaoService } from './../services/certidao.service';
import { ErrorHandlerService } from './error-handler.service';

registerLocaleData(localePt, 'pt-BR');

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule,

    MessagesModule
  ],
  providers: [
    CertidaoService,
    ErrorHandlerService,

    MessageService,

    Title,
    { provide: LOCALE_ID, useValue: 'pt-BR' }
  ],
  exports: [
    MessagesModule
  ]
})
export class CoreModule { }
