export class Interessado {
    tipo: Tipo;
    nomeRazaoSocial: string;
    cpfCnpj: string;
}

export enum Tipo {
    DIARIAS = 'Diárias',
    SUP_FUNDOS = 'Suprimento de Fundos',
    DIV_RESP = 'Diversos Responsáveis',
    CONVENIOS = 'Convênios'
}

export enum SituacaoDocumento {
    ATIVO = 'Ativo',
    INATIVO = 'Inativo',
    INEXISTENTE = 'Inexistente'
}

export class Certidao {
    tipo: Tipo;
    nomeRazaoSocial: string;
    cpfCnpj: string;
    codigoControle: string;
    dataHoraEmissao: Date;
    dataValidade: Date;
    status: string;
    situacaoDocumento: SituacaoDocumento;
    saldo: number;
    pendencias: Array<Pendencia>;
}

export class Pendencia {
    convenio: string;
    ug: string;
    data: Date;
    situacao: string;
    valor: number;
}