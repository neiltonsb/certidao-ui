import { Title } from '@angular/platform-browser';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Captcha } from 'primeng/captcha';

import { ValidateBrService } from 'angular-validate-br';

import { Certidao, Tipo } from './../core/model';
import { CertidaoService } from './../services/certidao.service';
import { ErrorHandlerService } from './../core/error-handler.service';

@Component({
  selector: 'app-autenticacao',
  templateUrl: './autenticacao.component.html',
  styleUrls: ['./autenticacao.component.css']
})
export class AutenticacaoComponent implements OnInit {

  tipos = [ 
    { label: 'Diárias', value: 'DIARIAS' },
    { label: 'Diversos Responsáveis', value: 'DIV_RESP' },
    { label: 'Suprimento de Fundos', value: 'SUP_FUNDOS' },
    { label: 'Convênios', value: 'CONVENIOS' }
  ];

  progressBarStyle = {
    height: '6px',
    display: 'none'
  };

  maskCpfCnpj = '999.999.999-99';
  labelCpfCnpj = 'CPF';
  displayModalAutenticacao = false;

  
  formulario: FormGroup;
  certidao: Certidao;
  pt_BR: any;

  @ViewChild("recaptcha")
  captchaWidget: Captcha;

  constructor(
    private formBuilder: FormBuilder,
    private title: Title,

    private validateBrService: ValidateBrService,

    private certidaoService: CertidaoService,
    private errorHandler: ErrorHandlerService
  ) { }

  ngOnInit() {
    this.title.setTitle('Autênticação');
    this.formulario = this.formBuilder.group({
      tipo: ['DIARIAS', Validators.required],
      cpfCnpj: ['', [Validators.required, this.validateBrService.cpf]],
      codigoControle: ['', Validators.required],
      dataHoraEmissao: ['', Validators.required]
    });

    this.pt_BR = {
      firstDayOfWeek: 1,
      dayNames: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sabádo"],
      dayNamesShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
      dayNamesMin: ["Do","Se","Te","Qa","Qi","Se","Sa"],
      monthNames: [ "Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro" ],
      monthNamesShort: [ "Jan", "Fev", "Mar", "Abr", "Mai", "Jun","Jul", "Ago", "Set", "Out", "Nov", "Dez" ],
      today: 'Hoje',
      clear: 'Limpar'
    };
  }

  autenticar() {
    this.progressBarStyle.display = 'block';
    this.certidaoService.autenticar(this.formulario.value, this.captchaWidget.getResponse().toString())
      .then(certidao => {
        this.certidao = certidao;
        this.certidao.tipo = Tipo[this.certidao.tipo];

        this.displayModalAutenticacao = true;
      }).catch(error => {
        this.errorHandler.handle(error);
      }).finally(() => {
        this.progressBarStyle.display = 'none';
        this.captchaWidget.reset();
      });
  }

  onTipoSelecionado(event) {
    if (event.value === 'CONVENIOS') {
      this.maskCpfCnpj = '99.999.999/9999-99';
      this.labelCpfCnpj = 'CNPJ'

      this.formulario.get('cpfCnpj').setValidators([Validators.required, this.validateBrService.cnpj]);
    } else {
      this.maskCpfCnpj = '999.999.999-99';
      this.labelCpfCnpj = 'CPF'

      this.formulario.get('cpfCnpj').setValidators([Validators.required, this.validateBrService.cpf]);
    }
  }
}
