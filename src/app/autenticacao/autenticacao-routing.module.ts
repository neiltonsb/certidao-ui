import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AutenticacaoComponent } from './autenticacao.component';

const routes: Routes = [
  { path: '', component: AutenticacaoComponent }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AutenticacaoRoutingModule { }
