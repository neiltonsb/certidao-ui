import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { CaptchaModule } from 'primeng/captcha';
import { CalendarModule } from 'primeng/calendar';
import { InputTextModule } from 'primeng/inputtext';
import { InputMaskModule } from 'primeng/inputmask';
import { ProgressBarModule } from 'primeng/progressbar';
import { SelectButtonModule } from 'primeng/selectbutton';

import { NgxPrintModule } from 'ngx-print';
import { AngularValidateBrLibModule } from 'angular-validate-br';

import { SharedModule } from './../shared/shared.module';
import { AutenticacaoComponent } from './autenticacao.component';
import { AutenticacaoRoutingModule } from './autenticacao-routing.module';

@NgModule({
  declarations: [AutenticacaoComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    CardModule,
    SelectButtonModule,
    InputTextModule,
    ButtonModule,
    InputMaskModule,
    CaptchaModule,
    ProgressBarModule,
    DialogModule,
    CalendarModule,

    AngularValidateBrLibModule,
    NgxPrintModule,

    SharedModule,
    AutenticacaoRoutingModule
  ]
})
export class AutenticacaoModule { }
