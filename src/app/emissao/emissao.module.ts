import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CardModule } from 'primeng/card';
import { SelectButtonModule } from 'primeng/selectbutton';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { InputMaskModule } from 'primeng/inputmask';
import { CaptchaModule } from 'primeng/captcha';
import { ProgressBarModule } from 'primeng/progressbar';
import { DialogModule } from 'primeng/dialog';

import { AngularValidateBrLibModule } from 'angular-validate-br';
import { NgxPrintModule } from 'ngx-print';

import { SharedModule } from './../shared/shared.module';
import { EmissaoComponent } from './emissao.component';
import { EmissaoRoutingModule } from './emissao-routing.module';

@NgModule({
  declarations: [EmissaoComponent,],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    CardModule,
    SelectButtonModule,
    InputTextModule,
    ButtonModule,
    InputMaskModule,
    CaptchaModule,
    ProgressBarModule,
    DialogModule,

    AngularValidateBrLibModule,
    NgxPrintModule,

    SharedModule,
    EmissaoRoutingModule
  ]
})
export class EmissaoModule { }
