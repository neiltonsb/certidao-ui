import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmissaoComponent } from './emissao.component';

const routes: Routes = [
  { path: '', component: EmissaoComponent }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class EmissaoRoutingModule { }
