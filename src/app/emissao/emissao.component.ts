import { Title } from '@angular/platform-browser';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Captcha } from 'primeng/captcha';

import { ValidateBrService } from 'angular-validate-br';

import { CertidaoService } from './../services/certidao.service';
import { Certidao, Tipo, SituacaoDocumento } from './../core/model';
import { ErrorHandlerService } from './../core/error-handler.service';


@Component({
  selector: 'app-emissao',
  templateUrl: './emissao.component.html',
  styleUrls: ['./emissao.component.css']
})
export class EmissaoComponent implements OnInit {

  tipos = [ 
    { label: 'Diárias', value: 'DIARIAS' },
    { label: 'Diversos Responsáveis', value: 'DIV_RESP' },
    { label: 'Suprimento de Fundos', value: 'SUP_FUNDOS' },
    { label: 'Convênios', value: 'CONVENIOS' }
  ];

  progressBarStyle = {
    height: '6px',
    display: 'none'
  };

  modalStyle = {
    left: '30px',
    right: '30px'
  };

  
  maskCpfCnpj = '999.999.999-99';
  labelCpfCnpj = 'CPF';
  captchaResponse = '';
  displayModalCertidao: boolean = false;
  displayModalAviso: boolean = false;
  
  formulario: FormGroup;
  certidao = new Certidao();

  @ViewChild("recaptcha")
  captchaWidget: Captcha;

  constructor(
    private formBuilder: FormBuilder,
    private title: Title,

    private validateBrService: ValidateBrService,

    private certidaoService: CertidaoService,
    private errorHandler: ErrorHandlerService
  ) { }


  ngOnInit() {
    this.title.setTitle('Emissão');
    this.formulario = this.formBuilder.group({
      tipo: ['DIARIAS', [Validators.required]],
      cpfCnpj: ['', [Validators.required, this.validateBrService.cpf]],
      nomeRazaoSocial: ['', [Validators.maxLength(45)]]
    });
  }

  emitir() {
    this.progressBarStyle.display = 'block';
    this.certidaoService.emitir(this.formulario.value, this.captchaWidget.getResponse().toString())
      .then(certidao => {
        this.certidao = certidao;

        this.certidao.tipo = Tipo[this.certidao.tipo];
        this.certidao.situacaoDocumento = SituacaoDocumento[this.certidao.situacaoDocumento];

        this.displayModalCertidao = true;
      }).catch(errorResponse => {
        if (errorResponse instanceof HttpErrorResponse
          && errorResponse.error instanceof Array 
          && errorResponse.error[0].type === 'InteressadoNaoEncontradoException') {
          this.displayModalAviso = true;
        } else {
          this.errorHandler.handle(errorResponse);
        }
      }).finally(() => {
        this.progressBarStyle.display = 'none';
        this.captchaWidget.reset();
      });
  }

  onTipoSelecionado(event) {
    if (event.value === 'CONVENIOS') {
      this.maskCpfCnpj = '99.999.999/9999-99';
      this.labelCpfCnpj = 'CNPJ'

      this.formulario.get('cpfCnpj').setValidators([Validators.required, this.validateBrService.cnpj]);
    } else {
      this.maskCpfCnpj = '999.999.999-99';
      this.labelCpfCnpj = 'CPF'

      this.formulario.get('cpfCnpj').setValidators([Validators.required, this.validateBrService.cpf]);
    }
  }

}
