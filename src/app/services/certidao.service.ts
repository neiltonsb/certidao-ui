import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Interessado, Certidao } from './../core/model';
import { environment } from './../../environments/environment';

@Injectable()
export class CertidaoService {


  certidoesUrl: string;

  constructor(private http: HttpClient) { 
    this.certidoesUrl = `${environment.apiUrl}`;
  }

  emitir(interessado: Interessado, response: string): Promise<Certidao> {
    let params = new HttpParams;
    params = params.append("g-recaptcha-response", response);


    return this.http.post<Certidao>(`${this.certidoesUrl}`, interessado, { params })
      .toPromise();
  }
  
  autenticar(certidao: Certidao, response: string): Promise<Certidao> {
    let params = new HttpParams;
    params = params.append("g-recaptcha-response", response);

    return this.http.post<Certidao>(`${this.certidoesUrl}?autenticar`, certidao, { params })
      .toPromise();
  }
}
