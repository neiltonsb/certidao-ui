import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManutencaoComponent } from './manutencao.component';
import { ManutencaoRoutingModule } from './manutencao-routing.module';

@NgModule({
  declarations: [ManutencaoComponent],
  imports: [
    CommonModule,

    ManutencaoRoutingModule
  ]
})
export class ManutencaoModule { }
