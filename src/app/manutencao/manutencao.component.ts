import { Title } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';

import { environment } from './../../environments/environment';

@Component({
  selector: 'app-manutencao',
  templateUrl: './manutencao.component.html',
  styleUrls: ['./manutencao.component.css']
})
export class ManutencaoComponent implements OnInit {

  manutencaoMessage: string;

  constructor(
    private title: Title
  ) { 
    this.manutencaoMessage = environment.manutencao.message;
  }

  ngOnInit() {
    this.title.setTitle('Estamos em Manutenção');
  }

}
