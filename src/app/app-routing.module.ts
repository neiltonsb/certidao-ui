import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RouterGuard } from './router.guard';

const routes: Routes = [
  { 
    path: 'emissao', 
    loadChildren: 'src/app/emissao/emissao.module#EmissaoModule',
    canActivate: [RouterGuard]
   },
  { 
    path: 'autenticacao', 
    loadChildren: 'src/app/autenticacao/autenticacao.module#AutenticacaoModule',
    canActivate: [RouterGuard] 
  },
  { path: 'error', loadChildren: 'src/app/error/error.module#ErrorModule' },
  { path: 'manutencao', loadChildren: 'src/app/manutencao/manutencao.module#ManutencaoModule' },
  { 
    path: '', 
    loadChildren: 'src/app/home/home.module#HomeModule',
    canActivate: [RouterGuard] 
  },
  { path: '**', loadChildren: 'src/app/home/home.module#HomeModule' }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
