import { Title } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';

import { MessageService } from 'primeng/components/common/messageservice';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    private title: Title,

    private messageService: MessageService
  ) { }

  ngOnInit() {
    this.title.setTitle('SUPER | Certidão');
    this.messageService.clear();
  }

}
